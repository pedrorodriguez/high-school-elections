package bktallyvoter;

import java.io.*;


public class Officers implements Serializable
{
    private static String[] p = new String[4];
    private static String[] vp = new String[4];
    private static String[] t = new String[4];
    private static String[] rs = new String[4];
    private static String[] cs = new String[4];
    private static String line;
    public Officers()
    {
        p = new String[4];
        vp = new String[4];
        t = new String[4];
        rs = new String[4];
        cs = new String[4];
        line = null;
    }
    public void readOfficers(String fn)
    {
    	try {
    	BufferedReader input = new BufferedReader(new FileReader(fn));
    	int z = 0;
    	int j = 0;
    	//Read in presidential candidates
    	line = input.readLine();
    	z = line.indexOf(",");
    	p[0] = line.substring(0, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	p[1] = line.substring(z, j);
    	j = j + 1;
    	z = line.indexOf(",", j);
    	p[2] = line.substring(j, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	p[3] = line.substring(z, j);
    	//Read in vice presidential candidates
    	line = input.readLine();
    	j = 0;
    	z = 0;
    	z = line.indexOf(",");
    	vp[0] = line.substring(0, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	vp[1] = line.substring(z, j);
    	j = j + 1;
    	z = line.indexOf(",", j);
    	vp[2] = line.substring(j, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	vp[3] = line.substring(z, j);
    	//Read in treasurer candidates
    	line = input.readLine();
    	j = 0;
    	z = 0;
    	z = line.indexOf(",");
    	t[0] = line.substring(0, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	t[1] = line.substring(z, j);
    	j = j + 1;
    	z = line.indexOf(",", j);
    	t[2] = line.substring(j, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	t[3] = line.substring(z, j);
    	//Read in recording secretary candidates
    	line = input.readLine();
    	j = 0;
    	z = 0;
    	z = line.indexOf(",");
    	rs[0] = line.substring(0, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	rs[1] = line.substring(z, j);
    	j = j + 1;
    	z = line.indexOf(",", j);
    	rs[2] = line.substring(j, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	rs[3] = line.substring(z, j);
    	//Read in corresponding secretary candidates
    	line = input.readLine();
    	j = 0;
    	z = 0;
    	z = line.indexOf(",");
    	cs[0] = line.substring(0, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	cs[1] = line.substring(z, j);
    	j = j + 1;
    	z = line.indexOf(",", j);
    	cs[2] = line.substring(j, z);
    	z = z + 1;
    	j = line.indexOf(",", z);
    	cs[3] = line.substring(z, j);
    	input.close();
    	}
    	catch(FileNotFoundException e) {System.out.print("Officer file not found");}
    	catch(IOException e) {System.out.print("IO Exception in readOfficer");}
    }
    public Officers(String[] p, String[] vp, String[] t, String[] rs, String[] cs)
    {
    	this.p = p;
    	this.vp = vp;
    	this.t = t;
    	this.rs = rs;
    	this.cs = cs;
    }
    public String[] getP()
    {
    	return p;
    }
    public String[] getVP()
    {
    	return vp;
    }
    public String[] getT()
    {
    	return t;
    }
    public String[] getRS()
    {
    	return rs;
    }
    public String[] getCS()
    {
    	return cs;
    }
}

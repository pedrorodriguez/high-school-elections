/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package bktallyvoter;
import java.io.*;
import java.util.*;

/**
 *
 * @author PedroRodriguez
 */
public class TallyMethods
{
    private static String fileName;
    private static int id;
    private static ArrayList<StudentInfo> database;
    private static String p;
    private static String vp;
    private static String t;
    private static String cs;
    private static String rs;
    private static int[] pCount = new int[4];
    private static int[] vpCount = new int[4];
    private static int[] tCount = new int[4];
    private static int[] rsCount = new int[4];
    private static int[] csCount = new int[4];
    private static String[] choices = new String[5];
    private static String[] winners = new String[20];
    private static ArrayList<TallyMethods> results = new ArrayList<TallyMethods>();
    public void setFileName (String i )
    {
        fileName = i;
    }
    public void resetCounts()
    {
        pCount = new int[4];
        vpCount = new int[4];
        tCount = new int[4];
        rsCount = new int[4];
        csCount = new int[4];
    }
    public void setID(int i)
    {
        id = i;
    }
    public void setDatabase(ArrayList<StudentInfo> i)
    {
        database = i;
    }
    public void setP(String i)
    {
        p = i;
    }
    public void setVP(String i)
    {
        vp = i;
    }
    public void setT(String i)
    {
        t = i;
    }
    public void setCS(String i)
    {
        cs = i;
    }
    public void setRS(String i)
    {
        rs = i;
    }
    public void setWinners(String[] i)
    {
        winners = i;
    }
    public void setChoice(String aP, String aVP, String aT, String aRS, String aCS)
    {
        choices[0] = aP;
        choices[1] = aVP;
        choices[2] = aT;
        choices[3] = aRS;
        choices[4] = aCS;
    }
    public String[] getChoice()
    {
        return choices;
    }
    public String getFileName()
    {
        return fileName;
    }
    public int getID()
    {
        return id;
    }
    public ArrayList<StudentInfo> getDatabase()
    {
        return database;
    }
    public String getP()
    {
        return p;
    }
    public String getVP()
    {
        return vp;
    }
    public String getT()
    {
        return t;
    }
    public String getRS()
    {
        return rs;
    }
    public String getCS()
    {
        return cs;
    }
    public String[] getWinners()
    {
        return winners;
    }
    public TallyMethods()
    {
        fileName = null;
        id = 0;
        database = null;
        p = null;
        vp = null;
        t = null;
        cs = null;
        rs = null;
        winners = new String[30];
        choices = new String[5];
    }
    public TallyMethods(String aP, String aVP, String aT, String aRS, String aCS)
    {
        p = aP;
        vp = aVP;
        t = aT;
        rs = aRS;
        cs = aCS;
    }
    public void readFile(int j)
    {
        database = StudentInfo.importStudents();
        int totalStudents = database.size();
        int topIndex = totalStudents - 1;
        int i = 0;
        winners = new String[20];
        Officers list = new Officers();
        list.readOfficers("officers.txt");
        try{
            id = j;
            int a = 0;
            int b = 0;
            BufferedReader read = new BufferedReader(new FileReader(Integer.toString(id)));
            String line = read.readLine();
            a = line.indexOf(",");
            a++;
            a = line.indexOf(",", a);
            a++;
            a = line.indexOf(",", a);
            a++;
            b = a;
            a = line.indexOf(",", a);
            String aP = line.substring(b, a);
            a++;
            b = line.indexOf(",", a);
            String aVP = line.substring(a, b);
            b++;
            a = line.indexOf(",", b);
            String aT = line.substring(b, a);
            a++;
            b = line.indexOf(",", a);
            String aRS = line.substring(a, b);
            b++;
            a = line.indexOf(",", b);
            String aCS = line.substring(b, a);
            TallyMethods vote = new TallyMethods();
            vote.setChoice(aP, aVP, aT, aRS, aCS);
            results.add(vote);
            i++;
            } catch(FileNotFoundException e) {}
            catch(IOException e) {}
        topIndex = results.size() - 1;
        i = 0;
        int y = 0;
        String[] president = list.getP();
        String[] vicePresident = list.getVP();
        String[] treasurer = list.getT();
        String[] cSecretary = list.getCS();
        String[] rSecretary = list.getRS();
        while (i <= topIndex)
        {
            while (y <= 3)
            {
                if (results.get(i).getChoice()[0].equals(president[y]))
                {
                    pCount[y]++;
                }
                if (results.get(i).getChoice()[1].equals(vicePresident[y]))
                {
                    vpCount[y]++;
                }
                if (results.get(i).getChoice()[2].equals(treasurer[y]))
                {
                    tCount[y]++;
                }
                if (results.get(i).getChoice()[3].equals(rSecretary[y]))
                {
                    rsCount[y]++;
                }
                if (results.get(i).getChoice()[4].equals(cSecretary[y]))
                {
                    csCount[y]++;
                }
                y++;
            }
            i++;
        }
        winners[0] = president[0] + " " + pCount[0] + "Votes";
        winners[1] = president[1] + " " + pCount[1] + "Votes";
        winners[2] = president[2] + " " + pCount[2] + "Votes";
        winners[3] = president[3] + " " + pCount[3] + "Votes";
        winners[4] = vicePresident[0] + " " + vpCount[0] + "Votes";
        winners[5] = vicePresident[1] + " " + vpCount[1] + "Votes";
        winners[6] = vicePresident[2] + " " + vpCount[2] + "Votes";
        winners[7] = vicePresident[3] + " " + vpCount[3] + "Votes";
        winners[8] = treasurer[0] + " " + tCount[0] + "Votes";
        winners[9] = treasurer[1] + " " + tCount[1] + "Votes";
        winners[10] = treasurer[2] + " " + tCount[2] + "Votes";
        winners[11] = treasurer[3] + " " + tCount[3] + "Votes";
        winners[12] = rSecretary[0] + " " + rsCount[0] + "Votes";
        winners[13] = rSecretary[1] + " " + rsCount[1] + "Votes";
        winners[14] = rSecretary[2] + " " + rsCount[2] + "Votes";
        winners[15] = rSecretary[3] + " " + rsCount[3] + "Votes";
        winners[16] = cSecretary[0] + " " + csCount[0] + "Votes";
        winners[17] = cSecretary[1] + " " + csCount[1] + "Votes";
        winners[18] = cSecretary[2] + " " + csCount[2] + "Votes";
        winners[19] = cSecretary[3] + " " + csCount[3] + "Votes";
        choices[0] = "empty";
        choices[1] = "empty";
        choices[2] = "empty";
        choices[3] = "empty";
        choices[4] = "empty";
    }
    public static void exportSummary()
    {
        try {

        int i = 0;
        PrintWriter export = new PrintWriter(new FileOutputStream("Election Summary"));
        while (i <= 19)
        {
            if (i == 0)
            {
                export.println("_________________________");
                export.println("President Results");
            }
            if (i == 4)
            {
                export.println("_________________________");
                export.println("Vice President Results");
            }
            if (i == 8)
            {
                export.println("_________________________");
                export.println("Treasurer Results");
            }
            if (i == 12)
            {
                export.println("_________________________");
                export.println("Recording Secretary Results");
            }
            if (i == 16)
            {
                export.println("_________________________");
                export.println("Corresponding Secretary Results");
            }
            String out = winners[i];
            export.println(out);
            i++;
    	}
        export.close();
        }
        catch (IOException e) {System.out.print("export summary exception");}
    }
}

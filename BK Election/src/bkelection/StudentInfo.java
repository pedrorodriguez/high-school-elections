package bkelection;

import java.io.*;
import java.util.*;

/**
 *
 * @author PedroRodriguez
 * This Program is created to run elections using file io
 */
public class StudentInfo implements Serializable {
    //Student Information Variables
	private String firstName;
    private String lastName;
    private int id;
    //Student Officer Selection Variables
    private String president;
    private String vicePresident;
    private String treasurer;
    private String recordingSecretary;
    private String secretary; /* this is the corresponding secretary*/
    private boolean voted = false;
    private static boolean almostID = false;
    private static boolean almostFN = false;
    private static boolean almostLN = false;
    /**
     * Sets all the values for Student and Office selections
     */
    public void setAll(String aFirstName, String aLastName, int aID, String aPresident, String aVicePresident, String aTreasurer, String aRecordingSecretary, String aSecretary)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        president = aPresident;
        vicePresident = aVicePresident;
        treasurer = aTreasurer;
        recordingSecretary = aRecordingSecretary;
        secretary = aSecretary;
        voted = true;
        almostID = false;
        almostFN = false;
        almostLN = false;
    }
    /**
     * Creates an empty student object
     */
    public StudentInfo()
    {
        firstName = null;
        lastName = null;
        id = 0;
        voted = false;
        almostID = false;
        almostFN = false;
        almostLN = false;
    }
    /**
     * Creates a student object with info and office selection values
     * @param String FirstName
     * @param String LastName
     * @param Int ID
     * @param String President
     * @param String Vice President
     * @param String Treasurer
     * @param String Recording Secretary
     * @param String Corresponding Secretary
     */
    public StudentInfo(String aFirstName, String aLastName, int aID, String aPresident, String aVicePresident, String aTreasurer, String aRecordingSecretary, String aSecretary)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        president = aPresident;
        vicePresident = aVicePresident;
        treasurer = aTreasurer;
        recordingSecretary = aRecordingSecretary;
        secretary = aSecretary;
        voted = true;
        almostID = false;
        almostLN = false;
        almostFN = false;
    }
    /**
     * Creates student object with info variables
     * @param String First name
     * @param String Last name
     * @param Int ID
     */
    public StudentInfo(String aFirstName, String aLastName, int aID)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        voted = false;
        almostID = false;
        almostFN = false;
        almostLN = false;
    }
    /**
     * @return String First Name
     */
    public String getFirstName()
    {
        return firstName;
    }
    /**
     * @return true if all fields but ID are recognized in StudentInfo Arraylist
     * returns false otherwise
     */
    public static boolean getAlmostID()
    {
        return almostID;
    }
    /**
     * @return true if all fields but First name are recognized in StudentInfo Arraylist
     * returns false otherwise
     */
    public static boolean getAlmostFN()
    {
        return almostFN;
    }
    /**
     * @return true if all fields but Last Name are recognized in StudentInfo Arraylist
     * returns false otherwise
     */
    public static boolean getAlmostLN()
    {
        return almostLN;
    }
    /**
     * @return String Last Name
     */
    public String getLastName()
    {
        return lastName;
    }
    /**
     * @return Int ID
     */
    public int getID()
    {
        return id;
    }
    /**
     * @return String President
     */
    public String getPresident()
    {
        return president;
    }
    /**
     * @return String Vice President
     */
    public String getVicePresident()
    {
        return vicePresident;
    }
    /**
     * @return String Treasurer
     */
    public String getTreasurer()
    {
        return treasurer;
    }
    /**
     * @return String Recording Secretary
     */
    public String getRecordingSecretary()
    {
        return recordingSecretary;
    }
    /**
     * @return String Corresponding Secretary
     */
    public String getSecretary()
    {
        return secretary;
    }
    /**
     * @return boolean, true if voted, false if not voted
     */
    public boolean getVoted()
    {
        return voted;
    }
    /**
     * resets almostID to false
     */
    public static void resetAlmostID()
    {
        almostID = false;
    }
    /**
     * resets almostFN to false
     */
    public static void resetAlmostFN()
    {
        almostFN = false;
    }
    /**
     * resets almostLN to false
     */
    public static void resetAlmostLN()
    {
        almostLN = false;
    }
    public void setID(int aid)
    {
        id = aid;
    }
    public void setLastName(String aLastName)
    {
        lastName = aLastName;
    }
    public void setFirstName(String aFirstName)
    {
        firstName = aFirstName;
    }
    public void setPresident(String aPresident)
    {
        president = aPresident;
    }
    public void setVicePresident(String aVicePresident)
    {
        vicePresident = aVicePresident;
    }
    public void setTreasurer(String aTreasurer)
    {
        treasurer = aTreasurer;
    }
    public void setRecordingSecretary(String aRecordingSecretary)
    {
        recordingSecretary = aRecordingSecretary;
    }
    public void setSecretary(String aSecretary)
    {
        secretary = aSecretary;
    }
    public void setVoted(boolean value)
    {
        voted =  value;
    }
    /**
     * Exports the StudentInfo object, student to a text file in format below
     * Pedro,Rodriguez,14113,preschoice,vpchoice,tchoice,rschoice,cschoice,
     * @param student
     */
    public static void exportStudent(StudentInfo student)
    {
    	try {
    	String id = Integer.toString(student.getID());
    	String fn = student.getFirstName();
    	String ln = student.getLastName();
    	int idn = student.getID();
    	String p = student.getPresident();
    	String vp = student.getVicePresident();
    	String t = student.getTreasurer();
    	String cs = student.getSecretary();
    	String rs = student.getRecordingSecretary();
    	PrintWriter export = new PrintWriter(new FileOutputStream(id));
    	String out = fn + "," + ln + "," + idn + "," + p + "," + vp + "," + t + "," + rs + "," + cs + ",";
    	export.write(out);
    	export.close();
    	}
    	catch (IOException e) {System.out.print("IO Exception in exportStudents");}
    }
    /**
     * Parses Text file into seperate StudentInfo objects that are then stored to an ArrayList
     * @return ArrayList<StudentInfo> of students found in Students.txt in the fold local to the application
     */
    public static ArrayList<StudentInfo> importStudents()
    {
        ArrayList<StudentInfo> database = new ArrayList<StudentInfo>();
    	try {
    	BufferedReader in = new BufferedReader(new FileReader("Students.txt"));
    	int i = 0;
        int z;
        int j;
        String line;
        String substring;
        String fName;
        String lName;
        String idnString;
        int idn;
        line = in.readLine();
        while (line != null)
        {
            z = 0;
            j = 0;
            z = line.indexOf(",");
            fName = line.substring(0, z);
            fName = fName.toLowerCase();
            z = z + 1;
            j = line.indexOf(",", (z));
            lName = line.substring(z, j);
            lName = lName.toLowerCase();
            j = j + 1;
            z = line.indexOf(",", j);
            idnString = line.substring(j, z);
            idn = Integer.parseInt(idnString);
            StudentInfo student = new StudentInfo(fName, lName, idn);
            database.add(student);
            i++;
            line = in.readLine();
        }
        in.close();
        }
    	catch (FileNotFoundException e) {System.out.print("File Not Found Exception in importStudents");}
    	catch (IOException e) {System.out.print("IOException in importStudents");}
        return database;
    }
    /**
     * Verifies that the student entered in BKElectionView exists and that the information is correctly entered
     * @param ArrayList<StudentInfo> obtained from importStudents()
     * @param fName
     * @param lName
     * @param aID
     * @return if students exists, true, if not, false
     */
    public static boolean verify(ArrayList<StudentInfo> database, String fName, String lName, int aID)
    {
    	boolean verify = false;
        int totalStudents = database.size();
        int topIndex = totalStudents - 1;
        int i = 0;
        boolean verifyFN = false;
        boolean verifyLN = false;
        boolean verifyID = false;
        while (i <= topIndex)
        {
            if (fName.equals(database.get(i).getFirstName()))
            {
                verifyFN = true;
            }
            if (lName.equals(database.get(i).getLastName()))
            {
                verifyLN = true;
            }
            if (aID == database.get(i).getID())
            {
                verifyID = true;
            }
            if ((verifyFN == true) && (verifyLN == true) && (verifyID == false))
            {
                almostID = true;
            }
            if ((verifyFN == true) && (verifyLN == false) && (verifyID == true))
            {
                almostLN = true;
            }
            if ((verifyFN == false) && (verifyLN == true) && (verifyID == true))
            {
                almostFN = true;
            }
            if ((verifyFN == true) && (verifyLN == true) && (verifyID == true))
            {
                return true;
            }
            else{ verifyFN = false; verifyLN = false; verifyID = false;}
            i++;
        }
        return verify;
    }
}


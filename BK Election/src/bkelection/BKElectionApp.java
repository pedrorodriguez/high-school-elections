/*
 * BKElectionApp.java
 */

package bkelection;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import java.util.*;
import java.io.*;

/**
 * The main class of the application.
 */
public class BKElectionApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
        show(new BKElectionView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    ((java.awt.Frame)root).setResizable(false);
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of BKElectionApp
     */
    public static BKElectionApp getApplication() {
        return Application.getInstance(BKElectionApp.class);
    }
    private static String fn;
    private static String ln;
    private static int id;
    private static String p;
    private static String vp;
    private static String t;
    private static String cs;
    private static String rs;
    private static String[] pres;
    private static String[] vPres;
    private static String[] treas;
    private static String[] csec;
    private static String[] rsec;
    /**
     * @returns String First Name
     */
    public String getFN()
    {
        return fn;
    }
    /**
     * @return String Last Name
     */
    public String getLN()
    {
        return ln;
    }
    /**
     * @return Int ID
     */
    public int getID()
    {
        return id;
    }
    /**
     * @param String First Name
     */
    public void setFN(String i)
    {
        fn = i;
    }
    /**
     * @param String Last Name
     */
    public void setLN(String i)
    {
        ln = i;
    }
    /**
     * @param String ID#
     */
    public void setID(int i)
    {
        id = i;
    }
    /**
     * Main method launching the application.
     */
    public static void main(String[] args)
    {
        Officers asbOfficers = new Officers();
        asbOfficers.readOfficers("officers.txt");
        launch(BKElectionApp.class, args);
    }
}

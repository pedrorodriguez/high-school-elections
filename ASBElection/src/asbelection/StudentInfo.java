/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package asbelection;
import java.io.Serializable;

/**
 *
 * @author PedroRodriguez
 */
public class StudentInfo implements Serializable {
    private String firstName;
    private String lastName;
    private int id;
    private int password;
    private String president;
    private String vicePresident;
    private String treasurer;
    private String recordingSecretary;
    private String secretary;
    private boolean voted = false;
    public void setAll(String aFirstName, String aLastName, int aID, int aPassword, String aPresident, String aVicePresident, String aTreasurer, String aRecordingSecretary, String aSecretary)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        password = aPassword;
        president = aPresident;
        vicePresident = aVicePresident;
        treasurer = aTreasurer;
        recordingSecretary = aRecordingSecretary;
        secretary = aSecretary;
        voted = false;
    }
    public StudentInfo()
    {
        firstName = null;
        lastName = null;
        id = 0;
        password = 0;
        voted = false;
    }
    public StudentInfo(String aFirstName, String aLastName, int aID, int aPassword, String aPresident, String aVicePresident, String aTreasurer, String aRecordingSecretary, String aSecretary)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        password = aPassword;
        president = aPresident;
        vicePresident = aVicePresident;
        treasurer = aTreasurer;
        recordingSecretary = aRecordingSecretary;
        secretary = aSecretary;
        voted = false;
    }
    public StudentInfo(String aFirstName, String aLastName, int aID, int aPassword)
    {
        firstName = aFirstName;
        lastName = aLastName;
        id = aID;
        password = aPassword;
        voted = false;
    }
    public String getFirstName()
    {
        return firstName;
    }
    public String getLastName()
    {
        return lastName;
    }
    public int getID()
    {
        return id;
    }
    public int getPassword()
    {
        return password;
    }
    public String getPresident()
    {
        return president;
    }
    public String getVicePresident()
    {
        return vicePresident;
    }
    public String getTreasurer()
    {
        return treasurer;
    }
    public String getRecordingSecretary()
    {
        return recordingSecretary;
    }
    public String getSecretary()
    {
        return secretary;
    }
    public boolean getVoted()
    {
        return voted;
    }
    public void setPassword(int apassword)
    {
        password = apassword;
    }
    public void setID(int aid)
    {
        id = aid;
    }
    public void setLastName(String aLastName)
    {
        lastName = aLastName;
    }
    public void setFirstName(String aFirstName)
    {
        firstName = aFirstName;
    }
    public void setPresident(String aPresident)
    {
        president = aPresident;
    }
    public void setVicePresident(String aVicePresident)
    {
        vicePresident = aVicePresident;
    }
    public void setTreasurer(String aTreasurer)
    {
        treasurer = aTreasurer;
    }
    public void setRecordingSecretary(String aRecordingSecretary)
    {
        recordingSecretary = aRecordingSecretary;
    }
    public void setSecretary(String aSecretary)
    {
        secretary = aSecretary;
    }
    public void setVoted(boolean value)
    {
        voted =  value;
    }
}


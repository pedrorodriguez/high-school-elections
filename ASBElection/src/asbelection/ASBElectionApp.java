/*
 * ASBElectionApp.java
 */

package asbelection;

import java.io.*;
import java.util.*;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class ASBElectionApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
        show(new ASBLogin(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of ASBElectionApp
     */
    public static ASBElectionApp getApplication() {
        return Application.getInstance(ASBElectionApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args)
    {
        addStudents(importFile());
        launch(ASBElectionApp.class, args);
    }
    public static String fileName = "Students.txt";
    public static String line;
    public static ArrayList<String> Lines = new ArrayList<String>();
    public static ArrayList<StudentInfo> database = new ArrayList<StudentInfo>();
    public static ArrayList<String> importFile()
    {
        try {
            BufferedReader inputStream = new BufferedReader(new FileReader(fileName));
            while ((line = inputStream.readLine()) != null)
            {
                Lines.add(line);
            }
            inputStream.close();
        }
        catch (FileNotFoundException E){System.out.print("file not found"); System.exit(0);}
        catch (IOException e) {System.exit(0);}
        return Lines;
    }
    public static void addStudents(ArrayList<String> file)
    {
        int i = 0;
        int z;
        int j;
        String substring;
        String fName;
        String lName;
        String idnString;
        String passString;
        int idn;
        int pass;
        try {
        while ((line = file.get(i)) != null)
        {
            z = 0;
            j = 0;
            z = line.indexOf(",");
            fName = line.substring(0, z);
            z = z + 2;
            j = line.indexOf(",", (z));
            lName = line.substring(z, j);
            j = j + 2;
            z = line.indexOf(",", j);
            idnString = line.substring(j, z);
            idn = Integer.parseInt(idnString);
            z = z + 2;
            j = line.indexOf(",", z);
            passString = line.substring(z, j);
            pass = Integer.parseInt(passString);
            StudentInfo student = new StudentInfo(fName, lName, idn, pass);
            database.add(student);
            i++;
        }}
        catch (IndexOutOfBoundsException e) {}

    }
    public static ArrayList<StudentInfo> returnStudents()
    {
        return database;
    }
    public static void exportElection(ArrayList<String> file) throws Exception
    {
        ObjectOutputStream export = new ObjectOutputStream(new FileOutputStream("Current Election.dat"));
        int i = 0;
        int z;
        int j;
        String substring;
        String fName;
        String lName;
        String idnString;
        String passString;
        int idn;
        int pass;
        try {
        while ((line = file.get(i)) != null)
        {
            z = 0;
            j = 0;
            z = line.indexOf(",");
            fName = line.substring(0, z);
            z = z + 2;
            j = line.indexOf(",", (z));
            lName = line.substring(z, j);
            j = j + 2;
            z = line.indexOf(",", j);
            idnString = line.substring(j, z);
            idn = Integer.parseInt(idnString);
            z = z + 2;
            j = line.indexOf(",", z);
            passString = line.substring(z, j);
            pass = Integer.parseInt(passString);
            StudentInfo student = new StudentInfo(fName, lName, idn, pass);
            export.writeObject(student);
            i++;
        }
        export.close();
        }
        catch (IndexOutOfBoundsException e) {}
    }
    public static void testMethod()
    {
        StudentInfo testStudent = new StudentInfo();
        testStudent.setID(11111);
        testStudent.setPassword(222);
        testStudent.setFirstName("afirstname");
        testStudent.setLastName("alastname");
        try {
        ObjectOutputStream test = new ObjectOutputStream(new FileOutputStream("Current Election.dat"));
        test.writeObject(testStudent);
        test.close();
        }
        catch (FileNotFoundException e) {System.out.print("File not found");}
        catch (IOException e) {System.out.print("IO Exception");}
    }
    public static int login(String fName, String lName, int aID, int aPass, String President, String VicePresident, String Treasurer, String CorrespondingSecretary, String RecordingSecretary)
    {
        int verify = 99999;
        int totalStudents = database.size();
        int topIndex = totalStudents - 1;
        int i = 0;
        boolean verifyFN = false;
        boolean verifyLN = false;
        boolean verifyID = false;
        boolean verifyPass = false;
        while (i <= topIndex)
        {
            if (fName.equals(database.get(i).getFirstName()))
            {
                verifyFN = true;
            }
            if (lName.equals(database.get(i).getLastName()))
            {
                verifyLN = true;
            }
            if (aID == database.get(i).getID())
            {
                verifyID = true;
            }
            if (aPass == database.get(i).getPassword())
            {
                verifyPass = true;
            }
            if ((verifyFN == true) && (verifyLN == true) && (verifyID == true) && (verifyPass == true))
            {
                database.get(i).setPresident(President);
                database.get(i).setVicePresident(VicePresident);
                database.get(i).setTreasurer(Treasurer);
                database.get(i).setRecordingSecretary(RecordingSecretary);
                database.get(i).setSecretary(CorrespondingSecretary);
                database.get(i).setVoted(true);
                verifyFN = false;
                verifyLN = false;
                verifyID = false;
                verifyPass = false;
                return aID;
            }
            else{ verifyFN = false; verifyLN = false; verifyID = false; verifyPass = false;}
            i++;
        }
        return verify;
    }
    public static void exportRunningElection() throws Exception
    {
        ObjectOutputStream export = new ObjectOutputStream(new FileOutputStream("Current Election.dat"));
        int i = 0;
        int length = database.size();
        int j = length - 1;
        while (i <= j)
        {
            export.writeObject(database.get(i));
            System.out.print(i);
            i++;
        }
        export.close();
    }
    public static void exportRunningElectionArrayList() throws Exception
    {
        ObjectOutputStream export = new ObjectOutputStream(new FileOutputStream("Current Election.dat"));
        export.writeObject(database);
        export.close();
    }
}
